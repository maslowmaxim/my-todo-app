My todo app project. 

План (начальный)
1) заходишь сюда, покупаешь тачамбу Ubuntu https://clo.ru/about - готово
2) ставишь докер на нее - готово 
3) создаешь проект на gitlab - готово 
4) пилишь проект на яве, спринг бот все дела и сперва просто RestController с hello world - готово 
5) настраиваешь ci cd в гитлабе
   1) добавить докер файл для проекта 
      1) либо докерфайл
      2) либо сбилдить образ при помощи spring-boot:build-image - готово
      3) либо добавить jib 
   2) установить на сервер гитлаб ранер (на докер экзекьюторе) который будет ранить джобы - готово 
   3) добавить .gitlab-ci.yml с 3-мя стейджами - готово
      1) test
      2) build_and_push
      3) deploy
         1) сначла решить вопрос с коннектом по ssh и просто запуском докер образа
         2) затем уже переделеываем на запуск с докер компоусом и подменой версии образа который будем запускать  (можно кстати наебать всех и билдить 2 образа - latest и тегнутый а затем в докер-композе всегда запускать латест)
   4) выяснииь по какой причине иногда ябается пайплайн с командной mvn not found или что то такое
   5) разобрать список статей который я перерыл чтобы систематизировать полученные знания 
   6) настроить фаервол для порта 8080
6) поправить чтобы ci работал нормально, а не через раз и организовать его работу через докер экзекьюторы
7) переписать на использование докер-компоус файла в качестве описателя для запуска
8) поставить портейнер и через него запускаться 